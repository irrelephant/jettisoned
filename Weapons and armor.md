# Weapons and armor #

Balance sheet:
https://docs.google.com/spreadsheets/d/1mYHMRo1awSWrp8lbC-qcgMgz6BbKzCYpsgl6VMGbQU4/edit?usp=sharing

Penetration occures when following condition evaluates so true:

sin(a) * Shot Speed in KUpS^2 * Caliber >= Material Strength

When block is penetrated it is considered destroyed instantly.


Spaceship armor:
- Metal blocks (basic protection hull);
- Metal screen (supplementary protection hull);
- Composite material (in case of a hull repairing. Composite material has special properties of its aggregate state)
