﻿using UnityEngine;

public class BallisticsCalculator
{
    public const float GlancingAngleComponent = 0.8f;

    public static void ProcessTileHit(RaycastHit2D collision, float remainingDeltaTime, Projectile projectile)
    {
        Object.Destroy(projectile.gameObject, 5f);
        var collisionNormal = collision.normal;
        var angleComponent = Mathf.Abs(Vector3.Dot(projectile.Velocity.normalized, collisionNormal));
        var energy = (projectile.Velocity / 1000).sqrMagnitude * projectile.Caliber * angleComponent;

        var isGlancing = angleComponent < GlancingAngleComponent;
        if (isGlancing) energy *= 0.6f;

        var tilePoint = Point.FromVector(collision.collider.transform.position);
        var tileId = Ship.Current.Layout.SafeGet(tilePoint);

        if (tileId.HasValue)
        {
            var targetTile = Ship.Current.GetTileAt(tilePoint);

            if (targetTile.Resistance > energy)
            {
                targetTile.Damage(energy);
                if (isGlancing)
                {
                    var newVelocity = Vector3.Reflect(projectile.Velocity, collisionNormal) * 0.4f;
                    var deviatedVelocity = Vector3.Lerp(newVelocity, collisionNormal, Random.value * 0.2f);
                    var modifiedCollisionPoint = Vector2.Lerp(projectile.transform.position, collision.point, 0.999f);
                    projectile.ReflectAt(deviatedVelocity, modifiedCollisionPoint, remainingDeltaTime);
                }
                else
                {
                    projectile.AbsorbHitAt(collision.point);
                }
            }
            else
            {
                ShatterTile(targetTile, projectile, collision.point);
            }
        }
    }

    private static void ShatterTile(TileEntity targetTile, Projectile projectile, Vector3 hitPosition)
    {
        Ship.Current.DestroyTileAt(targetTile.Position);
        projectile.Shatter(hitPosition);
    }
}