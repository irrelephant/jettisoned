﻿using UnityEngine;

public static class LayerHelper
{
    public const int PlayerLayer = 8;
    public const int TilesLayer = 9;

    public const int TilesLayerMask = 1 << TilesLayer;
    public const int PlayerLayerMask = 1 << PlayerLayer;

    public const float ProjectileOffsetZ = -2;
    public const float CameraOffsetZ = -4;

    public static bool IsTile(GameObject gameObject)
    {
        return gameObject.layer == TilesLayer;
    }

    public static Vector3 AsProjectilePosition(this Vector3 position)
    {
        position.z = ProjectileOffsetZ;
        return position;
    }

    public static Vector3 AsProjectilePosition(this Vector2 position)
    {
        return new Vector3(position.x, position.y, ProjectileOffsetZ);
    }

    public static Vector3 AsCameraPosition(this Vector3 position)
    {
        return new Vector3(position.x, position.y, CameraOffsetZ);
    }
}
