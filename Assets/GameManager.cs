﻿using UnityEngine;

public class GameManager : MonoBehaviour
{
    public TilePrefabLibrary TilePrefabsLibrary;

    public ProjectileTypeLibrary ProjectileTypeLibrary;

    public static GameManager Instance;


    public void OnEnable()
    {
        Instance = this;
    }
}