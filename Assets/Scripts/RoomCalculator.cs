﻿using System.Collections.Generic;
using System.Linq;

public static class RoomCalculator
{
    public static void CalculateInitialRooms(Ship ship)
    {
        var maxY = ship.Layout.GetUpperBound(0);
        var maxX = ship.Layout.GetUpperBound(1);

        ship.RoomsLayout = new int[maxY + 1, maxX + 1];

        for (int x = 0; x <= maxX; x++)
        {
            for (int y = 0; y <= maxY; y++)
            {
                var tile = ship.Layout.SafeGet(x, y);
                bool tileTofill = tile.HasValue && tile.Value != 0 && Tile.GetById(tile.Value).IsPassable(x, y);
                if (tileTofill && ship.RoomsLayout.SafeGet(x, y) == 0)
                {
                    InitialRoomFill(ship, x, y);
                }
            }
        }
    }

    public static void RecalculateRoomsNearPoint(Ship ship, Point p)
    {
        var invalidatedRoomId = ship.RoomsLayout.SafeGet(p).Value;
        var invalidatedRoom = ship.RoomsData[invalidatedRoomId];

        foreach (var neighbor in p.GetManhattanNeighbors())
        {
            var tile = ship.Layout.SafeGet(neighbor);
            bool tileTofill = tile.HasValue && tile.Value != 0 && Tile.GetById(tile.Value).IsPassable(neighbor);
            if (tileTofill && ship.RoomsLayout.SafeGet(neighbor) == invalidatedRoomId)
            {
                RecursiveFill(ship, neighbor.X, neighbor.Y, invalidatedRoom);
            }
        }

        invalidatedRoom.Invalidate();
    }

    private static void MarkAsToBeFilledIfNeeded(int x, int y, List<Point> toFill, List<Point> filled)
    {
        var point = new Point(x, y);
        if (!filled.Contains(point) && !toFill.Contains(point))
        {
            toFill.Add(point);
        }
    }

    private static void InitialRoomFill(Ship ship, int x, int y)
    {
        var filledTiles = new List<Point>();
        var tilesToBeFilled = new List<Point> { new Point(x, y) };

        bool vacuum = false;

        while (tilesToBeFilled.Any())
        {
            var tilePoint = tilesToBeFilled.First();
            tilesToBeFilled.RemoveAt(0);
            var tile = ship.Layout.SafeGet(tilePoint.X, tilePoint.Y);

            if (tile.HasValue)
            {
                if (tile.Value == Tile.NoneId)
                {
                    vacuum = true;
                } else if (Tile.GetById(tile.Value).IsPassable(tilePoint))
                {
                    filledTiles.Add(tilePoint);

                    MarkAsToBeFilledIfNeeded(tilePoint.X + 1, tilePoint.Y, tilesToBeFilled, filledTiles);
                    MarkAsToBeFilledIfNeeded(tilePoint.X - 1, tilePoint.Y, tilesToBeFilled, filledTiles);
                    MarkAsToBeFilledIfNeeded(tilePoint.X, tilePoint.Y + 1, tilesToBeFilled, filledTiles);
                    MarkAsToBeFilledIfNeeded(tilePoint.X, tilePoint.Y - 1, tilesToBeFilled, filledTiles);
                }
            }
            else
            {
                vacuum = true;
            }
        }

        RoomData roomData;
        if (vacuum)
        {
            roomData = Ship.OuterSpace;
        }
        else
        {
            roomData = new RoomData(ship);
            ship.RoomsData[roomData.Id] = roomData;
        }

        foreach (var filledTile in filledTiles)
        {
            ship.RoomsLayout.SafeSet(filledTile, roomData.Id);
            roomData.Tiles.Add(filledTile);
        }

        if (roomData != Ship.OuterSpace) roomData.RemainingAirVolume = roomData.Volume;
    }

    private static void RecursiveFill(Ship ship, int x, int y, RoomData originalRoom)
    {
        var filledTiles = new List<Point>();
        var tilesToBeFilled = new List<Point> { new Point(x, y) };

        RoomData roomData = new RoomData(ship);
        ship.RoomsData[roomData.Id] = roomData;

        while (tilesToBeFilled.Any())
        {
            var tilePoint = tilesToBeFilled.First();
            tilesToBeFilled.RemoveAt(0);
            var tile = ship.Layout.SafeGet(tilePoint);

            if (tile.HasValue)
            {
                if (tile.Value == Tile.NoneId)
                {
                    roomData.AddDepressurizationFront(tilePoint);
                } else if (Tile.GetById(tile.Value).IsPassable(tilePoint))
                {
                    filledTiles.Add(tilePoint);

                    MarkAsToBeFilledIfNeeded(tilePoint.X + 1, tilePoint.Y, tilesToBeFilled, filledTiles);
                    MarkAsToBeFilledIfNeeded(tilePoint.X - 1, tilePoint.Y, tilesToBeFilled, filledTiles);
                    MarkAsToBeFilledIfNeeded(tilePoint.X, tilePoint.Y + 1, tilesToBeFilled, filledTiles);
                    MarkAsToBeFilledIfNeeded(tilePoint.X, tilePoint.Y - 1, tilesToBeFilled, filledTiles);
                }
            }
        }

        foreach (var filledTile in filledTiles)
        {
            ship.RoomsLayout.SafeSet(filledTile, roomData.Id);
            roomData.Tiles.Add(filledTile);
        }

        roomData.RemainingAirVolume = roomData.Volume;
        roomData.RemainingAirVolume = roomData.Volume * originalRoom.AirflowCoefficient;

        if (roomData.DepressurizationFronts.Any())
        {
            AirflowCalculator.RecalculateAirflow(ship, new Point(x, y));
            roomData.Depressurize();
        }
    }
}