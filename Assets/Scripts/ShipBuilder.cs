﻿using UnityEngine;

public class ShipBuilder : MonoBehaviour
{
    public Texture2D shipBitmap;
    
    private GameObject BuildShipAt(Vector3 origin, Ship ship)
    {
        var shipObj = new GameObject("Ship");

        for (int x = 0; x <= ship.Layout.GetUpperBound(1); x++)
        {
            for (int y = 0; y <= ship.Layout.GetUpperBound(0); y++)
            {
                var current = ship.Layout.SafeGet(x, y);
                if (current.HasValue && current.Value != 0)
                {
                    PlaceBlock(shipObj, origin, x, y, current.Value);
                }
            }
        }

        return shipObj;
    }

    
    public static GameObject PlaceBlock(GameObject ship, Vector3 origin, int xOffset, int yOffset, int pieceId)
    {
        var piece = GameManager.Instance.TilePrefabsLibrary.GetById(pieceId);
        var gameObj = Instantiate(piece, origin + Vector3.up * yOffset + Vector3.right * xOffset, Quaternion.identity);
        gameObj.name = xOffset + ";" + yOffset;
        gameObj.transform.SetParent(ship.transform);
        return gameObj;
    }

    private void FillAirCurrentMatrix(Ship ship)
    {
        var maxY = ship.Layout.GetUpperBound(0);
        var maxX = ship.Layout.GetUpperBound(1);

        ship.AirCurrents = new Vector3[maxY + 1, maxX + 1];
    }

    public void Awake()
    {
        if (shipBitmap != null)
        {
            // Add 2 cell padding to the ship containing matrices to make sure thereare no issues with border blocks of the ship
            var shipTileMatrix = new int[shipBitmap.height + 4, shipBitmap.width + 4];
            var maxY = shipTileMatrix.GetUpperBound(0);
            var maxX = shipTileMatrix.GetUpperBound(1);

            var realYellow = new Color(1f, 1f, 0f, 1f);

            for (int x = 2; x <= maxX - 2; x++)
            {
                for (int y = 2; y <= maxY - 2; y++)
                {
                    var pixelColor = shipBitmap.GetPixel(x - 2, y - 2);
                    if (pixelColor == Color.white)
                    {
                        shipTileMatrix[y, x] = Tile.Floor.Id;
                    } else if (pixelColor == Color.black)
                    {
                        shipTileMatrix[y, x] = Tile.Wall.Id;
                    } else if (pixelColor == Color.green)
                    {
                        shipTileMatrix[y, x] = Tile.Door.Id;
                    } else if (pixelColor == Color.blue)
                    {
                        shipTileMatrix[y, x] = Tile.FloorLight.Id;
                    } else if (pixelColor == Color.magenta)
                    {
                        shipTileMatrix[y, x] = Tile.TurretBlock.Id;
                    } else if (pixelColor == realYellow)
                    {
                        shipTileMatrix[y, x] = Tile.DiagonalWall.Id;
                    }
                }
            }

            Ship.Current = new Ship { Layout = shipTileMatrix };
        }

        Ship.OuterSpace = new RoomData(Ship.Current, true);

        var reactor = FindObjectOfType<ReactorModule>();
        if (reactor != null)
        {
            Ship.Current.Reactor = reactor;
        }
        else
        {
            Debug.LogWarning("NO REACTOR!!!! OMG!");
        }

        Ship.Current.RoomsData[Ship.OuterSpace.Id] = Ship.OuterSpace;

        RoomCalculator.CalculateInitialRooms(Ship.Current);
        FillAirCurrentMatrix(Ship.Current);

        Ship.Current.ShipObj = BuildShipAt(Vector3.zero, Ship.Current);
        Ship.Current.ColorRooms();

        foreach (var module in FindObjectsOfType<Module>())
        {
            var stateModule = module as TriStateModule;
            if (stateModule != null)
            {
                stateModule.SetActive();
            }
            var weapon = module as WeaponryModule;
            if (weapon != null)
            {
                weapon.Boot();
            }
        }
    }
}