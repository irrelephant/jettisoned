﻿using UnityEngine;

public class InputManager : MonoBehaviour
{
    public ControllableEntity[] Targets;
    private int currentTargetIndex;

    public ControllableEntity Target
    {
        get { return Targets[currentTargetIndex]; }
    }

    public static InputManager Instance { get; private set; }

    public void Start()
    {
        Instance = this;
    }

    public void Update()
    {
        var lookDirection = CameraManager.Instance.PlayerLookDirection;
        Target.transform.rotation = Quaternion.AngleAxis(Mathf.Atan2(lookDirection.y, lookDirection.x) * Mathf.Rad2Deg - 90, Vector3.forward);

        if (Input.GetMouseButtonUp(0))
        {
            DestroyTileOnClick();
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            SwitchToNextTarget();
        }

        var vel = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        if (vel.magnitude > 1f) vel.Normalize();

        if (Target != null)
        {
            Target.Walk(vel);
        }
    }

    private void SwitchToNextTarget()
    {
        currentTargetIndex++;
        if (currentTargetIndex == Targets.Length) currentTargetIndex = 0;
        CameraManager.Instance.Tracking = Target.transform;
    }

    public void DestroyTileOnClick()
    {
        var rc = CameraManager.Instance.RaycastToMousePosition();

        if (rc)
        {
            var clickTarget = rc.transform.gameObject;
            if (!LayerHelper.IsTile(clickTarget)) return;

            var module = clickTarget.GetComponent<Module>();
            if (module != null)
            {
                module.OnInteract();
            }
            else
            {
                var tileEntity = Ship.Current.GetTileAt(Point.FromVector(clickTarget.transform.position));
                if (tileEntity is TileEntityDoor)
                {
                    TileDoor.ToggleDoor(tileEntity as TileEntityDoor);
                }
                else
                {
                    Ship.Current.DestroyTileAt(Point.FromVector(clickTarget.transform.position));
                }
            }
        }
    }
}