﻿using UnityEngine;

public class ControllableEntity : MonoBehaviour
{
    public float WalkSpeed;

    public void Walk(Vector2 velocity)
    {
        transform.Translate(velocity * Time.deltaTime * WalkSpeed, Space.World);
    }
}
