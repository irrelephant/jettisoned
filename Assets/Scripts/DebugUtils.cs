﻿using UnityEngine;

public class DebugUtils
{
    public static void ColorEquipotential(Equipotential equipotential)
    {
        var color = Random.ColorHSV(0, 1, 1, 1, 1, 1);
        foreach (var point in equipotential.Points)
        {
            var tile = Ship.Current.GetTileObject(point);
            if (tile != null)
            {
                var ev = tile.GetComponent<EquipotentialView>();
                if (ev != null)
                {
                    ev.Color = color;
                    ev.Id = equipotential.Id;
                }
            }
        }
    }
}