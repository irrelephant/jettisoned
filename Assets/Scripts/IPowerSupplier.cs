﻿public interface IPowerSupplier
{
    /// <summary>
    /// Connects the power consumer to the supply, requesting power every update
    /// </summary>
    /// <param name="consumer">consumer to supply power to</param>
    /// <param name="amountPerSecond">amount of power requested per second of game time. Call with a value &lt;0 to disconnect</param>
    void ConnectConsumer(IPowerConsumer consumer, float amountPerSecond);

    /// <summary>
    /// Disconnects the consumer from the power supply
    /// </summary>
    /// <param name="consumer"></param>
    void DisconnectConsumer(IPowerConsumer consumer);
}
