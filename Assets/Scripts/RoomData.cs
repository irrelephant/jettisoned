﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class RoomData
{
    private static int nextUniqueId = 1;
    public int Id;

    public List<Point> Tiles;
    public int Volume { get { return this == Ship.OuterSpace ? Int32.MaxValue : Tiles.Count; } }
    public float RemainingAirVolume;

    public float AirflowCoefficient
    {
        get { return RemainingAirVolume / Volume; }
    }

    public Color Color;
    public Ship Ship;
    public bool Depressurized;
    public List<Equipotential> DepressurizationFronts;

    private IEnumerator depressurizationRoutine;

    public RoomData(Ship ship, bool depressurized = false)
    {
        Id = nextUniqueId++;
        Tiles = new List<Point>();
        Depressurized = depressurized;
        DepressurizationFronts = new List<Equipotential>();
        Ship = ship;
        Color = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);

        if (!Depressurized) RemainingAirVolume = Volume;
    }

    public void Merge(params RoomData[] otherRooms)
    {
        foreach (var otherRoom in otherRooms)
        {
            RemainingAirVolume += otherRoom.RemainingAirVolume;
            Tiles.AddRange(otherRoom.Tiles);
            DepressurizationFronts.AddRange(otherRoom.DepressurizationFronts);

            if (!Depressurized && DepressurizationFronts.Any())
            {
                Depressurize();
            }

            foreach (var tile in otherRoom.Tiles)
            {
                Ship.RoomsLayout.SafeSet(tile, Id);
            }
        }
    }

    public static RoomData NewRoomAt(Point p, Ship ship)
    {
        var newRoom = new RoomData(ship);
        newRoom.Tiles.Add(p);
        ship.RoomsLayout.SafeSet(p, newRoom.Id);
        ship.RoomsData[newRoom.Id] = newRoom;
        newRoom.RemainingAirVolume = 1f;
        return newRoom;
    }

    public void AddDepressurizationFront(params Point[] points)
    {
        var mergeFronts =
            DepressurizationFronts.Where(front =>
                front.Points.Any(frontPoint => points.Any(point => point.IsManhattanAdjacent(frontPoint)))).ToList();

        var newFront = Equipotential.CreateFront(this);

        foreach (var front in mergeFronts)
        {
            DepressurizationFronts.Remove(front);
            newFront.Points.AddRange(front.Points);
            newFront.TotalFlux += front.TotalFlux;
        }

        newFront.Points.AddRange(points);
        newFront.TotalFlux += points.Length;

        newFront.InitFromPointsList();
        DepressurizationFronts.Add(newFront);
    }

    private IEnumerator DepressurizeCoroutine()
    {
        var waitForTick = new WaitForFixedUpdate();
        while (RemainingAirVolume > 0)
        {
            RemainingAirVolume -= DepressurizationFronts.Sum(front => front.TotalFlux) * 0.05f;
            yield return waitForTick;
        }

        RemainingAirVolume = 0f;
        yield return null;
    }

    public void Depressurize()
    {
        Depressurized = true;
        depressurizationRoutine = DepressurizeCoroutine();
        CoroutineRunner.Instance.AttachCoroutine(depressurizationRoutine);
    }

    public void Invalidate()
    {
        if (depressurizationRoutine != null) CoroutineRunner.Instance.StopCoroutine(depressurizationRoutine);
        Ship.RoomsData.Remove(Id);
    }
}