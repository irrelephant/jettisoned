﻿using System.Collections.Generic;

public struct Equipotential
{
    public List<Point> Points;
    public int[,] EquipotentialMap;
    public float TotalFlux;
    public RoomData Room;

    private static int nextUniqueId = 1;

    public static int NextUniqueId
    {
        get { return nextUniqueId; }
    }

    public int Id;

    public static Equipotential None;

    public void InitFromPointsList()
    {
        foreach (var point in Points)
        {
            EquipotentialMap[point.Y, point.X] = Id;
        }
    }

    public Equipotential(RoomData room, List<Point> points, float totalFlux)
    {
        Id = nextUniqueId++;
        Points = points;
        EquipotentialMap = null;
        Room = room;
        TotalFlux = totalFlux;
    }

    public Equipotential(RoomData room)
    {
        Id = nextUniqueId++;
        Points = new List<Point>();
        EquipotentialMap = null;
        Room = room;
        TotalFlux = 0;
    }
    
    public static Equipotential CreateFront(RoomData room)
    {
        return new Equipotential(room)
        {
            EquipotentialMap = new int[room.Ship.RoomsLayout.GetUpperBound(0)+ 1, room.Ship.RoomsLayout.GetUpperBound(1) + 1]
        };
    }
}