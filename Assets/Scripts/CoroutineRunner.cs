﻿using System.Collections;
using UnityEngine;

public class CoroutineRunner : MonoBehaviour
{
    public static CoroutineRunner Instance { get; private set; }

    public void Awake()
    {
        Instance = this;
    }

    public void AttachCoroutine(IEnumerator coroutine)
    {
        StartCoroutine(coroutine);
    }
}