﻿#if UNITY_EDITOR
using UnityEditor;
#endif

using UnityEngine;

public class TileView : MonoBehaviour
{
#if UNITY_EDITOR
    public void OnDrawGizmos()
    {
        var point = Point.FromVector(transform.position);
        var room = Ship.Current.GetRoomAt(point);
        if (room == null) return;

        Handles.Label(transform.position + Vector3.up * 0.5f - Vector3.right * 0.5f, point + "\n" +
            "@" + room.Id + 
            "\n" + room.AirflowCoefficient);

        Gizmos.DrawLine(transform.position, transform.position + Ship.Current.AirCurrents[point.Y, point.X] * 0.5f);
    }
#endif
}