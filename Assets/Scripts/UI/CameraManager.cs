﻿using UnityEngine;

public class CameraManager : MonoBehaviour
{
    public Camera ControlledCamera;
    public Transform Tracking;

    public float CameraDistance = 8f;
    public float CameraLerpSpeed = 0.2f;
    public float CameraMouseFollowDistance = 0.2f;

    [HideInInspector]
    public Vector3 PlayerLookPosition;

    [HideInInspector]
    public Vector3 PlayerLookDirection;

    public static CameraManager Instance { get; private set; }

    public void Start()
    {
        Instance = this;
    }

    public void Update()
    {
        transform.position = Tracking.position;//Vector3.Lerp(transform.position, Tracking.position, CameraLerpSpeed);
        CalculatePlayerLook();
        var desiredPosition = Vector3.Lerp(Tracking.position, PlayerLookPosition, CameraMouseFollowDistance);
        ControlledCamera.transform.position = Vector3.Lerp(ControlledCamera.transform.position, desiredPosition, CameraLerpSpeed).AsCameraPosition();
        ControlledCamera.orthographicSize = Mathf.Lerp(ControlledCamera.orthographicSize, CameraDistance, CameraLerpSpeed);
    }

    private void CalculatePlayerLook()
    {
        PlayerLookPosition = ControlledCamera.ScreenToWorldPoint(Input.mousePosition);
        PlayerLookDirection = PlayerLookPosition - transform.position;
        PlayerLookDirection.z = 0;
        PlayerLookDirection.Normalize();
    }

    public RaycastHit2D RaycastToMousePosition()
    {
        var position = ControlledCamera.ScreenToWorldPoint(Input.mousePosition);
        var ray = ControlledCamera.ScreenPointToRay(Input.mousePosition);

        return Physics2D.Raycast(position, ray.direction);
    }
}