﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AirflowCalculator
{
    public static void RecalculateAirflow(Ship ship, Point startingPoint)
    {
        bool first = true;

        int minimalFrontId = Equipotential.NextUniqueId;
        foreach (var front in ship.GetRoomAt(startingPoint).DepressurizationFronts)
        {
            var currentEquipotential = front;
            while (currentEquipotential.Points.Any())
            {
                var nextEquipotential = BuildNextEquipotential(ship, front, currentEquipotential, minimalFrontId);
                CalculateAirflowVectors(ship, front, nextEquipotential, currentEquipotential, first);
                currentEquipotential = nextEquipotential;
            }

            first = false;
        }
    }

    private static Equipotential BuildNextEquipotential(Ship ship, Equipotential front, Equipotential currentEquipotential, int minimalEquipotentialId)
    {
        var nextEquipotential = new Equipotential(front.Room);

        foreach (var point in currentEquipotential.Points)
        {
            for (int i = 0; i < 4; i++)
            {
                var delta = Point.ManhattanDeltas[i];
                var adjacentPoint = new Point(point.X + delta.X, point.Y + delta.Y);
                var adjacentEquipotentialId = front.EquipotentialMap[adjacentPoint.Y, adjacentPoint.X];
                if (adjacentEquipotentialId <= minimalEquipotentialId &&
                    IsPassableTile(ship, adjacentPoint))
                {
                    nextEquipotential.Points.Add(adjacentPoint);
                    front.EquipotentialMap[adjacentPoint.Y, adjacentPoint.X] = nextEquipotential.Id;
                }
            }
        }

        nextEquipotential.TotalFlux = currentEquipotential.TotalFlux * 0.95f;
        return nextEquipotential;
    }

    private static void CalculateAirflowVectors(Ship ship, Equipotential front, Equipotential nextEquipotential, Equipotential currentEquipotential, bool first)
    {
        foreach (var adjacent in nextEquipotential.Points)
        {
            var flowDirection = Vector3.zero;
            var currentPoint = adjacent.ToVector3();

            for (int i = 0; i < 4; i++)
            {
                var delta = Point.ManhattanDeltas[i];
                var targetPoint = new Point(adjacent.X + delta.X, adjacent.Y + delta.Y);
                if (front.EquipotentialMap[targetPoint.Y, targetPoint.X] == currentEquipotential.Id)
                {
                    flowDirection += targetPoint.ToVector3() - currentPoint;
                }
            }

            flowDirection.Normalize();
            var currentAtPoint = first ? Vector3.zero : (ship.AirCurrents.SafeGet(adjacent) ?? Vector3.zero);
            ship.AirCurrents.SafeSet(adjacent, currentAtPoint + flowDirection * currentEquipotential.TotalFlux / nextEquipotential.Points.Count);
        }
    }
    private static bool IsPassableTile(Ship ship, Point point)
    {
        var tile = ship.Layout.SafeGet(point);
        if (tile.HasValue)
        {
            var tileObj = Tile.GetById(tile.Value);
            return tileObj != null && tileObj.IsPassable(point);
        }

        return false;
    }

}