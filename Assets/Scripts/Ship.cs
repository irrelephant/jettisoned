﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Ship
{
    public static Ship Current;
    public static RoomData OuterSpace;

    public GameObject ShipObj;
    public Vector3 Origin;

    public int[,] Layout;
    public int[,] RoomsLayout;
    public Vector3[,] AirCurrents;

    public TriStateModule Engine;
    public IPowerSupplier Reactor;
    public IDictionary<int, RoomData> RoomsData = new Dictionary<int, RoomData>();
    public IDictionary<Point, TileEntity> UniqueTiles = new Dictionary<Point, TileEntity>();

    public RoomData GetRoomAt(Point p)
    {
        return GetRoomAt(p.X, p.Y);
    }

    public RoomData GetRoomAt(int x, int y)
    {
        int? roomId = RoomsLayout.SafeGet(x, y);
        var tileId = Layout.SafeGet(x, y);

        if (tileId.HasValue)
        {
            var tile = Tile.GetById(tileId.Value);
            if (tile == null) return OuterSpace;
            if (!tile.IsPassable(x, y)) return null;
        }

        if (!roomId.HasValue)
        {
            return OuterSpace;
        }

        if (roomId != 0)
        {
            return RoomsData[roomId.Value];
        }

        return OuterSpace;
    }

    public TileEntity GetTileAt(Point p)
    {
        if (UniqueTiles.ContainsKey(p))
        {
            return UniqueTiles[p];
        }

        var newTile = Layout.SafeGet(p);
        if (newTile.HasValue)
        {
            var tile = Tile.GetById(newTile.Value);
            var newTe = tile.NewTileEntity(p);
            UniqueTiles[p] = newTe;
            return newTe;
        }

        return null;
    }

    public void MergeAdjacentRooms(Point p)
    {
        var roomsToMerge = p
            .GetManhattanNeighbors()
            .GroupBy(GetRoomAt)
            .Where(group => group.Key != null)
            .ToList();

        var outerSpaceGrouping = roomsToMerge.FirstOrDefault(grouping => grouping.Key == OuterSpace);
        if (outerSpaceGrouping != null)
        {
            roomsToMerge.Remove(outerSpaceGrouping);
        }

        var mainRoom = roomsToMerge.MaxBy(group => group.Key.Volume);
        roomsToMerge.Remove(mainRoom);

        if (mainRoom != null)
        {
            MergeAdjacentRooms(p, mainRoom.Key, roomsToMerge.Select(group => group.Key));
            AddDepressurizationFront(mainRoom.Key, outerSpaceGrouping);
        }
        else
        {
            var room = RoomData.NewRoomAt(p, this);
            AddDepressurizationFront(room, outerSpaceGrouping);
        }

        AirflowCalculator.RecalculateAirflow(this, p);
    }

    private void AddDepressurizationFront(RoomData room, IEnumerable<Point> points)
    {
        if (room != null && points != null)
        {
            room.AddDepressurizationFront(points.ToArray());
            room.Depressurize();
        }
    }

    private void MergeAdjacentRooms(Point p, RoomData mainRoom, IEnumerable<RoomData> roomsToMerge)
    {
        mainRoom.Tiles.Add(p);
        mainRoom.RemainingAirVolume += 1f;
        RoomsLayout.SafeSet(p, mainRoom.Id);

        var roomDatas = roomsToMerge.ToArray();
        if (roomDatas.Any())
        {
            mainRoom.Merge(roomDatas);
            foreach (var mergedRoom in roomDatas)
            {
                mergedRoom.Invalidate();
            }
        }
    }

    public void DestroyTileAt(Point p)
    {
        var tile = GetTileAt(p);
        if (tile != null)
        {
            UniqueTiles.Remove(p);
            var replacementId = tile.GetReplacementOnDestructionId();
            if (replacementId != Tile.NoneId)
            {
                Layout[p.Y, p.X] = replacementId;
                var tileObj = GetTileObject(p);
                Object.DestroyImmediate(tileObj.gameObject);
                Object.Instantiate(GameManager.Instance.TilePrefabsLibrary.GetById(replacementId),
                                   p.ToVector3(), Quaternion.identity,ShipObj.transform);
            }

            if (replacementId == Tile.NoneId || Tile.GetById(replacementId).IsPassable(p))
            {
                MergeAdjacentRooms(p);
            }
        }
    }

    public Transform GetTileObject(Point p)
    {
        Collider2D hitCollider = Physics2D.OverlapPoint(p.ToVector2());
        if (hitCollider)
        {
            return hitCollider.transform;
        }
        return null;
    }

    public Transform GetTileObject(int x, int y)
    {
        return GetTileObject(new Point(x, y));
    }

    public void ColorRooms()
    {
        for (int x = 0; x <= RoomsLayout.GetUpperBound(1); x++)
        {
            for (int y = 0; y <= RoomsLayout.GetUpperBound(0); y++)
            {
                var tile = Layout.SafeGet(x, y);
                var tileObj = GetTileObject(x, y);
                if (!tile.HasValue || tile.Value == 0) continue;

                if (Tile.GetById(tile.Value).IsPassable(new Point(x, y)))
                {
                    // tileObj.GetComponent<SpriteRenderer>().color = Color.grey;
                }
                else
                {
                    tileObj.GetComponent<SpriteRenderer>().color = Color.white;
                }
            }
        }
    }
}
