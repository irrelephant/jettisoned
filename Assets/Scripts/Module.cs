﻿using UnityEngine;

public abstract class Module : MonoBehaviour
{
    public int Health;
    public int MaxHealth;

    public virtual void Start()
    {
        Health = MaxHealth;
    }

    public abstract void Update();

    public abstract void OnInteract();
}
