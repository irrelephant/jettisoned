﻿public class TileWall : Tile
{
    public TileWall(int id) : base(id) { }

    public override bool IsPassable(Point p)
    {
        return false;
    }
}