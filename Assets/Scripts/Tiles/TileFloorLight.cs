﻿public class TileFloorLight : TileFloor
{
    public TileFloorLight(int id) : base(id) { }

    public override bool IsPassable(Point p)
    {
        return true;
    }
}