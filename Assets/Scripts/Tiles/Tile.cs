﻿public abstract class Tile
{
    public int Id;
    public static int NoneId = 0;
    public static TileWall Wall = new TileWall(1);
    public static TileFloor Floor = new TileFloor(2);
    public static TileDoor Door = new TileDoor(3);
    public static TileFloorLight FloorLight = new TileFloorLight(4);
    public static TileTurretHardpoint TurretBlock = new TileTurretHardpoint(5);
    public static TileWall DiagonalWall = new TileWall(6);

    private static readonly Tile[] Tiles =
    {
        null,
        Wall,
        Floor,
        Door,
        FloorLight,
        TurretBlock,
        DiagonalWall
    };

    public static Tile GetById(int id)
    {
        if (id >= Tiles.Length || id < 0)
            return null;
        return Tiles[id];
    }

    protected Tile(int id)
    {
        Id = id;
    }

    public abstract bool IsPassable(Point pos);

    public bool IsPassable(int x, int y)
    {
        return IsPassable(new Point(x, y));
    }

    public virtual TileEntity NewTileEntity(Point position)
    {
        return new TileEntity { Position = position, Health = 10, Resistance = 40 };
    }
}
