﻿using UnityEngine;

public class TileTurretHardpoint : InteractableTile
{
    public TileTurretHardpoint(int id) : base(id) { }

    public override bool IsPassable(Point p)
    {
        return false;
    }

    public override TileEntity NewTileEntity(Point position)
    {
        return new TileEntityTurretHardpoint
        {
            Position = position,
            Health = 20
        };
    }
    
    public override void OnInteract()
    {
        Debug.Log("Interacted with a gun!");
    }
}