﻿public class TileEntity
{
    public float Health;

    public float Resistance;

    public Point Position;

    public void Damage(float energy)
    {
        Health -= energy;
        Resistance -= energy / 2;

        if (Health <= 0)
        {
            Ship.Current.DestroyTileAt(Position);
        }
    }

    public int GetReplacementOnDestructionId()
    {
        return Tile.Floor.Id;
    }

}