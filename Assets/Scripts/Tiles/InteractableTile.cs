﻿public abstract class InteractableTile : Tile
{
    protected InteractableTile(int id) : base(id) { }

    public abstract void OnInteract();
}
