﻿public class TileFloor : Tile
{
    public TileFloor(int id) : base(id) { }

    public override bool IsPassable(Point p)
    {
        return true;
    }
}