﻿using UnityEngine;

public class TileDoor : InteractableTile
{
    public TileDoor(int id) : base(id) { }

    public override bool IsPassable(Point p)
    {
        var tile = Ship.Current.GetTileAt(p);
        if (tile != null)
        {
            return ((TileEntityDoor) tile).Open;
        }

        return false;
    }

    public override TileEntity NewTileEntity(Point position)
    {
        return new TileEntityDoor
        {
            Position = position,
            Health = 20,
            Open = false
        };
    }

    public static void ToggleDoor(TileEntityDoor doorData)
    {
        doorData.Open = !doorData.Open;
        var doorObj = Ship.Current.GetTileObject(doorData.Position);
        doorObj.GetComponent<Collider2D>().isTrigger = doorData.Open;
        Debug.Log("Door @ " + doorData.Position + " is now " + (doorData.Open ? "open" : "closed"));
        doorObj.GetComponent<SpriteRenderer>().color = doorData.Open ? Color.gray : Color.white;

        if (doorData.Open)
        {
            Ship.Current.MergeAdjacentRooms(doorData.Position);
            doorObj.GetComponentInChildren<DoorLight>().SetMode(DoorLightMode.Inactive);
        }
        else
        {
            RoomCalculator.RecalculateRoomsNearPoint(Ship.Current, doorData.Position);
            doorObj.GetComponentInChildren<DoorLight>().SetMode(DoorLightMode.Warn);
        }
    }
    
    public override void OnInteract()
    {
        Debug.Log("Toggled door!");
    }
}