﻿using System;
using UnityEngine;

public struct Point
{
    public int X;
    public int Y;

    public Point(int x, int y)
    {
        X = x;
        Y = y;
    }

    public override string ToString()
    {
        return string.Format("({0},{1})", X, Y);
    }

    public override bool Equals(object other)
    {
        if (other is Point)
        {
            var otherPoint = (Point)other;
            return otherPoint.X == X && otherPoint.Y == Y;
        }

        return false;
    }

    public override int GetHashCode()
    {
        int hash = 13;
        hash = (hash * 7) + X;
        hash = (hash * 7) + Y;
        return hash;
    }

    public Point[] GetManhattanNeighbors()
    {
        return new[]
        {
            new Point(X + 1, Y),
            new Point(X, Y + 1),
            new Point(X - 1, Y),
            new Point(X, Y - 1)
        };
    }

    public static Point[] ManhattanDeltas = {
        new Point(1, 0),
        new Point(0, 1),
        new Point(-1, 0),
        new Point(0, -1) 
    }; 

    public Point[] GetEuclideanNeighbors()
    {
        return new[]
        {
            new Point(X + 1, Y),
            new Point(X + 1, Y + 1),
            new Point(X, Y + 1),
            new Point(X - 1, Y + 1),
            new Point(X - 1, Y),
            new Point(X - 1, Y - 1),
            new Point(X, Y - 1),
            new Point(X + 1, Y - 1)
        };
    }

    public Vector3 ToVector3()
    {
        return new Vector3(X, Y);
    }

    public Vector2 ToVector2()
    {
        return new Vector2(X, Y);
    }

    public Point Minus(Point other)
    {
        return new Point(X - other.X, Y - other.Y);
    }

    public bool IsManhattanAdjacent(Point other)
    {
        return Math.Abs(X - other.X) == 1 && Y == other.Y || Math.Abs(Y - other.Y) == 1 && X == other.X;
    }

    public bool IsEuclideanAdjacent(Point other)
    {
        return Math.Abs(X - other.X) < 2 && Math.Abs(Y - other.Y) < 2;
    }

    public static Point FromVector(Vector3 v3)
    {
        return new Point((int) Mathf.Round(v3.x), (int) Mathf.Round(v3.y));
    }

}