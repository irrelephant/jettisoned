﻿using UnityEngine;

public class Projectile : MonoBehaviour
{
    private float caliber;
    public float Caliber
    {
        get { return caliber; }
        set
        {
            caliber = value;
            trailRenderer.widthMultiplier = Mathf.Max((Mathf.Log(caliber) + 2f) / 15f, 0.15f);
        }
    }

    public GameObject SparkRenderer;

    public Vector3 Velocity;

    private LineRenderer trailRenderer;
    private Vector3[] prevPoints;
    private bool destroyOnNextFrame;

    public void Awake()
    {
        trailRenderer = GetComponent<LineRenderer>();
        Launch(Velocity);
    }

    public void ReflectAt(Vector3 velocity, Vector3 reflectionPoint, float remainingDeltaTime)
    {
        Velocity = velocity;
        transform.position = reflectionPoint;
        SpawnSparks(Vector3.Angle(Vector3.up, velocity));
        UpdateTrailCoordinates(reflectionPoint);
        ProcessMovement(remainingDeltaTime);
    }

    public void AbsorbHitAt(Vector3 point)
    {
        transform.position = point.AsProjectilePosition();
        UpdateTrailCoordinates(transform.position);
        SpawnSparks(Vector3.Angle(Vector3.up, Velocity));
        DestroyOnNextFrame();
    }

    private void SpawnSparks(float angle)
    {
        var sparks = Instantiate(SparkRenderer, transform.position, Quaternion.Euler(-90 - angle, 90, 0));
        Destroy(sparks, 1.2f);
    }

    public void Launch(Vector3 velocity)
    {
        transform.position = new Vector3(transform.position.x, transform.position.y, LayerHelper.ProjectileOffsetZ);
        Velocity = velocity;
        var currentPos = transform.position;
        prevPoints = new[] { currentPos, currentPos, currentPos, currentPos, currentPos };
    }

    public void OnCollision(RaycastHit2D collision, float remainingDeltaTime)
    {
        if (LayerHelper.IsTile(collision.collider.gameObject))
        {
            BallisticsCalculator.ProcessTileHit(collision, remainingDeltaTime, this);
        }
        else
        {
            DestroyOnNextFrame();
            Debug.Log("We hit a non-tile object... not really sure what to do :C");
        }
    }

    public void DestroyOnNextFrame()
    {
        destroyOnNextFrame = true;
        Destroy(gameObject, 0.1f);
    }

    public void Update()
    {
        if (destroyOnNextFrame || OutOfCombatArea())
        {
            UpdateTrailCoordinates(transform.position);
        }
        else
        {
            ProcessMovement(Time.deltaTime);
        }

        trailRenderer.SetPositions(prevPoints);
    }

    private void ProcessMovement(float deltaTime)
    {
        var hitTest = Physics2D.Raycast(transform.position, Velocity, Velocity.magnitude * deltaTime, LayerHelper.TilesLayerMask);
        if (hitTest)
        {
            var remainingDeltaTime =
                deltaTime - (new Vector2(transform.position.x, transform.position.y) - hitTest.point).magnitude /
                Velocity.magnitude;
            OnCollision(hitTest, remainingDeltaTime);
        }
        else
        {
            MoveProjectile();
        }
    }

    public void MoveProjectile()
    {
        transform.Translate(Velocity * Time.deltaTime);
        UpdateTrailCoordinates(transform.position);
    }

    private void UpdateTrailCoordinates(Vector3 nextCoordinate)
    {
        prevPoints[4] = prevPoints[3];
        prevPoints[3] = prevPoints[2];
        prevPoints[2] = prevPoints[1];
        prevPoints[1] = Vector2.Lerp(prevPoints[2], nextCoordinate, 0.95f).AsProjectilePosition();
        prevPoints[0] = nextCoordinate;
    }

    private bool OutOfCombatArea()
    {
        return transform.position.sqrMagnitude > 10000000;
    }

    public void Shatter(Vector3 shatterPosition)
    {
        int shrapnelPieces = 2 + (int)(3 * Random.value);
        for (int i = 0; i < shrapnelPieces; i++)
        {
            var caliber = (0.5f + 0.5f * Random.value) * Caliber / shrapnelPieces;
            var shrapnelVelocity = Velocity * (0.2f + 0.8f * Random.value) / shrapnelPieces;

            if (caliber < 5f || shrapnelVelocity.sqrMagnitude < 225f)
            {
                continue;
            }

            var shrapnel = Instantiate(gameObject, shatterPosition.AsProjectilePosition(), Quaternion.identity).GetComponent<Projectile>();
            shrapnel.Caliber = caliber;
            
            shrapnel.Launch(Quaternion.Euler(0f, 0f, 60f * Random.value - 30f) * shrapnelVelocity);
            Destroy(shrapnel.gameObject, 2f);
        }
        transform.position = shatterPosition.AsProjectilePosition();
        UpdateTrailCoordinates(transform.position);
        DestroyOnNextFrame();
    }
}
