﻿public interface IPowerConsumer
{
    /// <summary>
    /// Called on a consumer when the requested power has been supplied. The logic is called on 'LateUpdate'
    /// </summary>
    /// <param name="requested">amount of power requested</param>
    /// <param name="supplied">amount of power supplied</param>
    /// <param name="supplier">supplier module</param>
    void OnPowerSupplied(float requested, float supplied, IPowerSupplier supplier);
}