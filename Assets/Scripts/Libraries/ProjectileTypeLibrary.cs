﻿using UnityEngine;

[CreateAssetMenu(menuName = "Irrelephant/ProjectileTypeLibrary")]
public class ProjectileTypeLibrary : ScriptableObject
{
    public Projectile DefaultProjectile;

}