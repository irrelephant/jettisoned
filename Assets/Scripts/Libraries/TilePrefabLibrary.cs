﻿using UnityEngine;

[CreateAssetMenu(menuName = "Irrelephant/TilePrefabLibrary")]
public class TilePrefabLibrary : ScriptableObject
{
    public GameObject[] TilePrefabs;

    public GameObject GetById(int id)
    {
        return TilePrefabs[id];
    }
}