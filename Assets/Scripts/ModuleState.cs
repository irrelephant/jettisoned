﻿public enum ModuleState
{
    Disabled, Working, Idle
}