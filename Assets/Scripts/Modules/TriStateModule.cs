﻿#if UNITY_EDITOR
using UnityEditor;
#endif

public class TriStateModule : Module, IPowerConsumer
{
    public float Efficiency { get; private set; }

    public float EnergyRequired = 3f;
    public float EnergyRequiredStandby = 1f;

    public ModuleState State { get; private set; }
    
    public void SetActive()
    {
        State = ModuleState.Working;
        Ship.Current.Reactor.ConnectConsumer(this, EnergyRequired);
    }

    public void SetIdle()
    {
        State = ModuleState.Idle;
        Ship.Current.Reactor.ConnectConsumer(this, EnergyRequiredStandby);
    }

    public void SetDisabled()
    {
        State = ModuleState.Disabled;
        Ship.Current.Reactor.DisconnectConsumer(this);
    }

    public override void OnInteract()
    {
        if (State == ModuleState.Working)
        {
            SetIdle();
        }
        else
        {
            SetActive();
        }
    }

    public void OnPowerSupplied(float requested, float supplied, IPowerSupplier supplier)
    {
        if (supplied < EnergyRequiredStandby)
        {
            SetDisabled();
        }
        else
        {
            Efficiency = supplied / requested;
        }
    }

#if UNITY_EDITOR
    public void OnDrawGizmos()
    {
        Handles.Label(transform.position, "State: " + State + "\n" + (Efficiency < 1f ? "Efficiency: " + Efficiency.ToString("P") : "Nominal"));
    }
#endif

    public override void Update() { }
}