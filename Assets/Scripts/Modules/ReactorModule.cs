﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class ReactorModule : Module, IPowerSupplier
{
    public float energy = 20f;
    public float MaxEnergy = 20f;
    public float MaxEnergyPerSecond = 10f;

    private float totalNeeded;
    private readonly IDictionary<IPowerConsumer, float> powerRequests = new Dictionary<IPowerConsumer, float>();

    private readonly IDictionary<IPowerConsumer, float> connectionRequests = new Dictionary<IPowerConsumer, float>();
    private readonly IList<IPowerConsumer> disconnectionRequests = new List<IPowerConsumer>();

    public override void Update()
    {
        if (Health <= 0) return;

        BuildUpEnergy();
        DistributeEnergy();
    }

    public void LateUpdate()
    {
        if (connectionRequests.Any())
        {
            foreach (var request in connectionRequests)
            {
                ConnectConsumerImmediate(request.Key, request.Value);
            }
            connectionRequests.Clear();
        }

        if (disconnectionRequests.Any())
        {
            foreach (var request in disconnectionRequests)
            {
                DisconnectConsumerImmediate(request);
            }
            disconnectionRequests.Clear();
        }
    }

    private void DistributeEnergy()
    {
        var neededThisTime = totalNeeded * Time.deltaTime;
        var coefficient = neededThisTime > energy ? energy / neededThisTime : 1f;
        energy -= neededThisTime > energy ? energy : neededThisTime;
        foreach (var powerRequest in powerRequests)
        {
            powerRequest.Key.OnPowerSupplied(powerRequest.Value * Time.deltaTime, powerRequest.Value * Time.deltaTime * coefficient, this);
        }
    }

    private void BuildUpEnergy()
    {
        var deltaEnergy = 1f * Health / MaxHealth * MaxEnergyPerSecond * Time.deltaTime;
        energy = Math.Min(MaxEnergy, energy + deltaEnergy);
    }

    public override void OnInteract()
    {
        Debug.Log("Trying to repair!");
        Health += 10;
        Health = Math.Min(MaxHealth, Health);
    }

    public void ConnectConsumer(IPowerConsumer consumer, float amountPerSecond)
    {
        if (amountPerSecond <= 0)
        {
            disconnectionRequests.Add(consumer);
        }
        else
        {
            connectionRequests.Add(consumer, amountPerSecond);
        }
    }

    private void ConnectConsumerImmediate(IPowerConsumer consumer, float amountPerSecond)
    {
        var alreadyConnected = powerRequests.ContainsKey(consumer);
        if (alreadyConnected) totalNeeded -= powerRequests[consumer];
        powerRequests[consumer] = amountPerSecond;
        totalNeeded += amountPerSecond;
    }

    public void DisconnectConsumer(IPowerConsumer consumer)
    {
        disconnectionRequests.Add(consumer);
    }

    private void DisconnectConsumerImmediate(IPowerConsumer consumer)
    {
        if (!powerRequests.ContainsKey(consumer)) return;

        totalNeeded -= powerRequests[consumer];
        powerRequests.Remove(consumer);
    }

#if UNITY_EDITOR
    public void OnDrawGizmos()
    {
        Handles.Label(transform.position, "Connected: " + powerRequests.Count + "\n" + "Consuming: " + totalNeeded + "MW/s" + "\n" + "Producing: " + 1f * Health / MaxHealth * MaxEnergyPerSecond + "MW/s");
    }
#endif
}