﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class WeaponryModule : Module, IPowerConsumer
{
    private float energyBuffer;
    public float EnergyPerShot = 100f;
    public float WeaponCooldown;

    public float EnergyDraw = 5f;

    private float lastShot;

    public override void OnInteract()
    {
        if (ReadyToShoot())
        {
            Shoot();
        }
    }

    private void Shoot()
    {
        Debug.Log("Wow shots fired!");
        lastShot = Time.time;
        energyBuffer -= EnergyPerShot;
        Ship.Current.Reactor.ConnectConsumer(this, EnergyDraw);
    }

    private bool ReadyToShoot()
    {
        return energyBuffer >= EnergyPerShot && Time.time - lastShot > WeaponCooldown;
    }

    public void OnPowerSupplied(float requested, float supplied, IPowerSupplier supplier)
    {
        energyBuffer += supplied;
        if (energyBuffer >= EnergyPerShot)
        {
            Debug.Log(name + " has gathered enough energy to shoot.");
            Ship.Current.Reactor.DisconnectConsumer(this);
        }
    }

    public void Boot()
    {
        Ship.Current.Reactor.ConnectConsumer(this, EnergyDraw);
    }


#if UNITY_EDITOR
    public void OnDrawGizmos()
    {
        if (ReadyToShoot())
        {
            Handles.Label(transform.position, "Ready");
        }
        else
        {
            var reloadingLeft = WeaponCooldown - (Time.time - lastShot);
            var bufferLeft = EnergyPerShot - energyBuffer;

            Handles.Label(transform.position,
                "Buffer: " + ((bufferLeft > 0) ? bufferLeft.ToString("####") + " MJ remaning" : "charged") + "\n" +
                (reloadingLeft > 0 ? "Reloading: " + reloadingLeft.ToString("####") + "sec." : "loaded"));
        }
    }
#endif

    public override void Update() { }
}