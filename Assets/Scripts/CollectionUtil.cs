﻿using System;
using System.Collections.Generic;
using System.Linq;

public static class CollectionUtil
{
    public static T? SafeGet<T>(this T[,] from, int x, int y) where T : struct
    {
        if (x < 0 || y < 0 || x > from.GetUpperBound(1) || y > from.GetUpperBound(0)) return null;
        return from[y, x];
    }

    public static T? SafeGet<T>(this T[,] from, Point p) where T : struct
    {
        return from.SafeGet(p.X, p.Y);
    }

    public static void SafeSet<T>(this T[,] to, int x, int y, T val)
    {
        if (x < 0 || y < 0 || x > to.GetUpperBound(1) || y > to.GetUpperBound(0)) return;
        to[y, x] = val;
    }

    public static void SafeSet<T>(this T[,] to, Point p, T val)
    {
        to.SafeSet(p.X, p.Y, val);
    }

    public static T MaxBy<T>(this IEnumerable<T> collection, Func<T, int> functor)
    {
        var currentMax = int.MinValue;
        T currentMaxItem = default(T);

        foreach (var item in collection)
        {
            var currentValue = functor(item);
            if (currentValue > currentMax)
            {
                currentMax = currentValue;
                currentMaxItem = item;
            }
        }

        return currentMaxItem;
    }
}