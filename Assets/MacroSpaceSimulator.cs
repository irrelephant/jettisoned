﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class MacroSpaceSimulator : MonoBehaviour
{
    public Vector2 RelativePosition;

    public Vector2 RelativeVelocity = new Vector2(125, 100f);

    public Text angleText;

    public float ShotDeviation = 3f;
    public float Angle = 90f;

    public float Distance
    {
        get { return RelativePosition.magnitude; }
    }

    public float AngularVelocity
    {
        get { return RelativeVelocity.magnitude / (2 * Mathf.PI) / Distance * Mathf.Rad2Deg; }
    }

    public float ShipRadius = 50f;
    public Vector3 ShipCenter = new Vector2(15, 50);

    public const float ShotTick = 0.25f;
    public static WaitForSeconds ShotTickDelay = new WaitForSeconds(ShotTick);

    public IEnumerator EnemyShoot(float distance, float speed, float angleZ)
    {
        float eta = distance / speed;

        while (eta / ShotTick > 2)
        {
            eta -= ShotTick;
            yield return ShotTickDelay;
        }

        yield return SpawnShotVolley(angleZ, speed, 50, 3f);
    }

    private IEnumerator SpawnShotVolley(float angleZ, float speed, int volleySize, float avgVolleyLen)
    {
        for (int i = 0; i < volleySize; i++)
        {
            var spawnOffset = Quaternion.Euler(0f, 0f, angleZ) * Vector3.up;
            var spawnPoint = ShipCenter + spawnOffset * (ShipRadius * 3 + 2 * speed);

            var shotVelocity = Quaternion.Euler(0f, 0f, angleZ + 180 - (ShotDeviation / 2f) + ShotDeviation * Random.value) * Vector3.up * speed;

            var shot = Instantiate(GameManager.Instance.ProjectileTypeLibrary.DefaultProjectile, spawnPoint.AsProjectilePosition(), Quaternion.identity);
            var projectile = shot.GetComponent<Projectile>();

            projectile.Launch(shotVelocity);
            projectile.Caliber = 10f;

            yield return new WaitForSeconds((0.8f + 0.4f * Random.value) * avgVolleyLen / 2 / volleySize);
        }
    }

    public IEnumerator AiTick()
    {
        Debug.Log("Started ai");
        while (true)
        {
            CoroutineRunner.Instance.AttachCoroutine(EnemyShoot(Distance, 1000, Angle));
            Debug.Log("Emeny fires");
            yield return new WaitForSeconds(2 + Random.value * 5);
        }
    }

    public void Start()
    {
        CoroutineRunner.Instance.AttachCoroutine(AiTick());
    }
}
