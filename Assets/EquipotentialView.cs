﻿#if UNITY_EDITOR
using UnityEditor;

#endif

using UnityEngine;

public class EquipotentialView : MonoBehaviour
{
    public Color Color;
    public int Id;
#if UNITY_EDITOR
    public void OnDrawGizmos()
    {
        Gizmos.color = Color;
        Gizmos.DrawCube(transform.position, new Vector3(0.3f, 0.3f, 0.3f));
        Handles.Label(transform.position, Id.ToString());
    }
#endif
}

