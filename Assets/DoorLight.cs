﻿using UnityEngine;

public enum DoorLightMode
{
    Inactive, Ok, Warn
}

public class DoorLight : MonoBehaviour
{
    public DoorLightMode Mode { get; private set; }

    private SpriteRenderer spriteRenderer;
    private new Light light;

    public void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        light = GetComponent<Light>();
        SetMode(DoorLightMode.Ok);
    }

    public void SetMode(DoorLightMode mode)
    {
        Mode = mode;

        switch (mode)
        {
            case DoorLightMode.Inactive:
            {
                light.enabled = false;
                spriteRenderer.enabled = false;
                return;
            }
            case DoorLightMode.Ok:
            {
                light.enabled = true;
                light.color = Color.green;
                spriteRenderer.enabled = true;
                spriteRenderer.color = Color.green;
                return;
            }
            case DoorLightMode.Warn:
            {
                light.enabled = true;
                light.color = Color.red;
                spriteRenderer.enabled = true;
                spriteRenderer.color = Color.red;
                return;
            }
        }
    }
}