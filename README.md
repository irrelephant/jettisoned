1. Core gameplay loop
	- How long is the game?
	- Win conditions?
	- Lose conditions?
	- What's happening during the game
	- How do we track player progress
	- Are there levels/difficulties?

2. Module behavior/list
	- What modules are there except for Engines, Reactor and Guns?
	- What guns do and how they work?
	- What does engine do?
	- What does ship's bridge do?

3. Crew
	- What each of the crew member does?
	- How they behave when not controlled by a player?
	- What gunners do except for shooting guns
	
4. Depressurization mechanics
	- How does depressurization affect the player? (Sound is disabled, obviously, but what are other downsides?)
	- Is it possible to restore the atmosphere in a room after it has been depressurized?

5. Ship design and enterior
	- What do we fill the ship with? As it stands there's plenty of space in the ship
	- What's the artstyle? The shape has been determined, but which color palette is used?
	
	Form and shape of the ship. Red = space, white = floors, black = walls, green = doors: 
Reference-style: 

![Shape of the ship][shape]

[shape]:http://i.imgur.com/oSJX7hI.png

	